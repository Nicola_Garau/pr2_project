package it.unica.pr2.progetto2015.g48974_48978_48985;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;
import java.util.ArrayList;
import java.util.Iterator;
import java.text.NumberFormat;

/**
 *	@author 48974 Frongia Nicola 
 *	@author 48978 Lepori Massimiliano 
 *	@author 48985 Garau Nicola
 *	@version 1.0, June 2015
 *	La classe Complessa contiene tre metodi e permette di implementare una funzione simile alla funzione AMMORT.ANNUO presente in LibreOffice.
 *	La funzione si occupa di calcolare l'ammortamento annuo secondo dei parametri forniti in input.
 *	La classe implementa l'interfaccia SheetFunction, della quale implementa i metodi "execute", "getCategory", "getHelp" e "getName".
 */

public class Complessa implements SheetFunction{

	private String category=("Finanza");
	private String help=("Restituisce l'ammortamento degressivo aritmetico per un determinato periodo. Questa funzione viene utilizzata per calcolare l'importo dell'ammortamento di un oggetto durante l'intero periodo di ammortamento. L'ammortamento digitale riduce la somma da ammortizzare di un importo costante di periodo in periodo. Sintassi AMMORT.ANNUO(Costo; Valore residuo; Vita utile; Periodo) Costo definisce il costo iniziale di un bene. Valore residuo è il valore di un bene a seguito del deprezzamento. Vita utile è il numero di periodi in cui il bene viene deprezzato, definito anche vita utile di un bene. Periodo definisce il periodo per il quale deve essere calcolata la svalutazione.");
	private String name=("AMMORT.ANNUO");

	/**
	 *	Costruttore della classe Complessa, non prende in ingresso alcun parametro.
	 */
	public Complessa(){}

	/**
	 *	Il metodo execute permette calcolare i valori dell'ammortamento secondo la vita utile e restituirli come output.
	 *	@param args Una lista di parametri in ingresso. In questo caso sono tre double: costo iniziale, valore residuo e vita utile.
	 *	@return Un oggetto di tipo Object contenente un vettore di Double rappresentante gli importi dell'ammortamento.
	 */
    	public Object execute(Object ... args){
		int i=0;
		int dim=10;

		Double costoIniziale=(((Double)args[0]).doubleValue());
		Double valoreResiduo=(((Double)args[1]).doubleValue());
		Double vitaUtile=(((Double)args[2]).doubleValue());
		Double importoDegressivo=(   2*( (costoIniziale-valoreResiduo) / (vitaUtile*(vitaUtile+1)) )   );		
		Double[] importoAmmortamento=new Double[dim];

		while(i<dim){
			importoAmmortamento[i]=((vitaUtile+1-(i+1))*importoDegressivo);
			i++;
		}
		
		return importoAmmortamento;
    	}
	
	/**
	 *	Il metodo getCategory restituisce una stringa contenente la tipologia della funzione implementata.
	 *	@return Stringa contenente la categoria della funzione implementata.
	 */
	public String getCategory(){
		return this.category;
	}

	/**
	 *	Il metodo getHelp restituisce una stringa contenente la documentazione della funzione LibreOffice implementata.
	 *	@return Stringa contenente la documentazione della funzione LibreOffice implementata.
	 */
	public String getHelp(){ 
		return this.help; }

	/**
	 *	Il metodo getName restituisce una stringa contenente il nome della funzione LibreOffice implementata.
	 *	@return Stringa contenente la documentazione della funzione LibreOffice implementata.
	 */
	public String getName(){
		return this.name;
	}
}
