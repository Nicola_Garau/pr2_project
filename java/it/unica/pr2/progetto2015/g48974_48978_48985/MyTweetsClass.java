package it.unica.pr2.progetto2015.g48974_48978_48985;
import java.util.List;
import twitter4j.*;
import twitter4j.conf.*;

/**
 *	@author 48974 Frongia Nicola 
 *	@author 48978 Lepori Massimiliano 
 *	@author 48985 Garau Nicola
 *	@version 1.0, June 2015
 *	La classe MyTweetsClass contiene tun metodo, il quale permette di leggere dei tweet dalla timeline di un utente Twitter.
 */

public class MyTweetsClass{

	/**
	 *	Costruttore della classe MyTweetsClass, non prende in ingresso alcun parametro.
	 */
	public MyTweetsClass(){}

	/**
	 *	Il metodo getTweets permette di estrarre gli ultimi 10 tweets dalla timeline di un utente e restituirli come output.
	 *	@param user Stringa contenente l'id dell'utente Twitter.
	 *	@param consumerKey Stringa contenente la Consumer Key.
	 *	@param consumerSecret Stringa contenente il Consumer Secret.
	 *	@param AccessToken Stringa contenente l'Access Token.
	 *	@param AccessTokenSecret Stringa contenente l'Access Token Secret.
	 *	@return Un oggetto di tipo String[] contenente gli ultimi 10 tweet dell'utente Twitter preceduti da una breve intestazione.
	 */
	public String[] getTweets(String user, String consumerKey, String consumerSecret, String AccessToken, String AccessTokenSecret){
		int dim=11;
		int i=0;
		String[] mioArray = new String[dim];
		List<Status> statuses;
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		  .setOAuthConsumerKey(consumerKey)
		  .setOAuthConsumerSecret(consumerSecret)
		  .setOAuthAccessToken(AccessToken)
		  .setOAuthAccessTokenSecret(AccessTokenSecret);
		TwitterFactory tf = new TwitterFactory(cb.build());
		Twitter twitter = tf.getInstance();

		for (i=1;i<dim;i++) {
			mioArray[i]=("- - -");
	    	}

		try {
			statuses = twitter.getUserTimeline(user);
			i=0;
		   	mioArray[0]=("Sto mostrando gli ultimi 10 tweet dell'utente " + user+".");
		    	i++;
		    	for (Status status : statuses) {
				if(i>=dim) break;
				mioArray[i]=(status.getText());
				i++;
		    	}
		} catch (TwitterException te) {
			mioArray[0]=("Non sono disponibili tweet dall'utente "+user+". Utente inesistente.");
		}
		return mioArray;
	}
}
